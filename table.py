class Table():
    
    def __init__(self):
        self.alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n","o", "p", "r", "s", "t", "u", "v", "x", "y", "w", "z", ".", ",", " "]
        self.numbers = list(range(28))
        self.table = dict(zip(self.alphabet, self.numbers))
        self.reverse_table = dict(zip(self.numbers, self.alphabet))
