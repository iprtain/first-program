from table import *
from encryption import *
from decryption import *

table = Table()
encryption = Encryption(table.table)
decryption = Decryption(table)

print("Supported symbols: a b c d e f g h i j k l m n o p r s t u v x y w z . , \n")
while True:
    
    try:
        user_choice = input("For encryption enter 1. For decryption enter 2: ")

        encryption.perform() if user_choice == "1" else decryption.perform()
        
    except KeyError:
        print("Please use only supported symbols! \n Supported symbols: a b c d e f g h i j k l m n o p r s t u v x y w z . , \n")










