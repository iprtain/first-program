class Encryption():
    def __init__(self, table):
        self.table = table
        self.keyword = None
        self.text_to_encrypt = None
        self.output = []

    def perform(self):
        self.keyword = input("Set your keyword: \n")
        self.text_to_encrypt = input("Enter text for encryption: \n").lower()

        for index in range(len(self.text_to_encrypt)):
            numeric_code = self.table[self.keyword[index % len(self.keyword)]] + self.table[self.text_to_encrypt[index]]
            if numeric_code > 28 : numeric_code -=28
                
            self.output.append(numeric_code)

        print(*self.output, sep=', ')

        self.output.clear()