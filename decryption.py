class Decryption():
    def __init__(self, table):
        self.reverse_table = table.reverse_table
        self.table = table.table
        self.keyword = None
        self.code_to_decrypt = None
        self.output = []

    def perform(self):
        self.keyword = input("Enter keyword: \n")
        self.code_to_decrypt = input("Enter code for decryption: \n").split(', ')
        
        for index in range(len(self.code_to_decrypt)):
            letter_number = int(self.code_to_decrypt[index]) - self.table[self.keyword[index % len(self.keyword)]]
            if letter_number < 0 : letter_number += 28

            letter = self.reverse_table[letter_number]
            self.output.append(letter)
        
        print(''.join(self.output))

        self.output.clear()
                
